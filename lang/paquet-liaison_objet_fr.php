<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/liaison_objet.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'liaison_objet_description' => 'Permet de lier des objets entre eux.',
	'liaison_objet_nom' => 'Liaison d’objets',
	'liaison_objet_slogan' => 'Lier vos objets spip'
);
